#include <boost/program_options.hpp>
#include <iostream>
#include <pthread.h>
#include <assert.h>

/* Namespaces */
namespace opt = boost::program_options;

/* Globals */
int MAX_COUNTER = 100;
int NUM_WORKERS = 1;
int counter = 0;

void *worker_func(void* args) {
	int thread_id = *((int*)args);
    int local_counter = 0;
    while (counter < MAX_COUNTER)
    {
        counter++;
        local_counter++;
    }
    pthread_exit((void*)local_counter);
}

int main(int argc, char *argv[]) {
    opt::options_description desc("All Options");
    desc.add_options()
        ("maxcounter", opt::value<int>(&MAX_COUNTER)->default_value(MAX_COUNTER), "integer-valued target value for the shared counter")
        ("workers", opt::value<int>(&NUM_WORKERS)->default_value(NUM_WORKERS), "integer-valued number of threads")
        ("help", "produce help message");

    // Parse the cmd line
    opt::variables_map vm;
    // Parse and store args
    opt::store(opt::parse_command_line(argc, argv, desc), vm);
    opt::notify(vm);
    if (vm.count("help")) {
        std::cout << desc << std::endl;
    }

    int total_local_counts = 0;
    pthread_t workers[NUM_WORKERS];
    int worker_counts[NUM_WORKERS];
    for (int i = 0; i < NUM_WORKERS; i++) {
        int result = pthread_create(&workers[i], NULL, worker_func,  &i);
        assert(!result);
    }
    for (int i = 0; i < NUM_WORKERS; i++) {
        int result = pthread_join(workers[i], (void**)&worker_counts[i]);
        assert(!result);
        total_local_counts += worker_counts[i];
    }
    for (int i = 0; i < NUM_WORKERS; i++) {
        std::cout << "Thread " << i + 1 << " local counter: " << worker_counts[i] << std::endl;
    }
    std::cout << "Local Count Total: " << total_local_counts << std::endl;
    std::cout << "Expected: " << MAX_COUNTER << std::endl;
    std::cout << "Result: " << counter << std::endl;
    return 0;
}